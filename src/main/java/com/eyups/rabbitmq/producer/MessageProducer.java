package com.eyups.rabbitmq.producer;

import com.eyups.rabbitmq.model.Message;
import com.eyups.rabbitmq.util.RabbitMq;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class MessageProducer {

    private final RabbitTemplate rabbitTemplate;

    public void sendToQueue(Message message) {
        System.out.println("Message Sent ID : " + message.getId());
        rabbitTemplate.convertAndSend(RabbitMq.DIRECT_EXCHANGE_NAME, RabbitMq.ORDERS_ROUTING_KEY, message);
    }
}
