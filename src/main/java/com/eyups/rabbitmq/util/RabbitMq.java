package com.eyups.rabbitmq.util;

public class RabbitMq {
    public static final String TOPIC_EXCHANGE_NAME = "ey-ups-Exchange-Topic";
    public static final String DIRECT_EXCHANGE_NAME = "ey-ups-Exchange-Direct";
    public static final String ORDERS_QUEUE_NAME = "ey-ups-Queue";
    public static final String ORDERS_ROUTING_KEY = "ey-ups-Routing";
}
