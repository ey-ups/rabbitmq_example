package com.eyups.rabbitmq.listener;

import com.eyups.rabbitmq.model.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class MessageListener2 {

    @RabbitListener(queues = "ey-ups-Queue")
    public void handleMessage(Message message) {
        System.out.println("Message handled.22."+message.toString());
    }
}
