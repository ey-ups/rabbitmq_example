package com.eyups.rabbitmq.configuration;

import com.eyups.rabbitmq.util.RabbitMq;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMqConfiguration {


    @Bean
    public Queue queue() {
        return new Queue(RabbitMq.ORDERS_QUEUE_NAME);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(RabbitMq.TOPIC_EXCHANGE_NAME);
    }
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(RabbitMq.DIRECT_EXCHANGE_NAME);
    }
    @Bean
    public Binding binding(Queue queue, DirectExchange eventExchange) {

        return BindingBuilder
                .bind(queue)
                .to(eventExchange)
                .with(RabbitMq.ORDERS_ROUTING_KEY);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
