package com.eyups.rabbitmq.controller;

import com.eyups.rabbitmq.model.Message;
import com.eyups.rabbitmq.producer.MessageProducer;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.stream.IntStream;

@RequiredArgsConstructor
@RestController
@RequestMapping("message")
public class MessageController {
    Logger logger = LoggerFactory.getLogger(MessageController.class);
    private final MessageProducer producer;

    @PostMapping
    public void getMessage(@RequestBody Message message) {
        IntStream.range(0, 5 )
                .forEach(index -> {
                    producer.sendToQueue(message);
                });

        //logger.info("selam");
    }

}
